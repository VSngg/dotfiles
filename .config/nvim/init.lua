require("plugins") require("options")
require("keymaps")
require("colorscheme")
require("lualine").setup()
require("bufferline").setup {
  options = {
    always_show_bufferline = false,
    show_close_icon = false,
    offsets = { { filetype = "NvimTree", text = "", padding = 1 } },
  }
}
require("nvim-tree").setup()
